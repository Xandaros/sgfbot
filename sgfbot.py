#!/usr/bin/env python3

import asyncio
import cairosvg
import discord
import hashlib
from hypercorn.asyncio import serve
import hypercorn.config
from quart import Quart, request, Response
from sgfmill import sgf, sgf_moves
from sgfmill.boards import Board
import toml
from typing import Any, Callable, Coroutine, Dict, List, Optional, Tuple

# https://discordapp.com/api/oauth2/authorize?client_id=577864454486622218&scope=bot&permissions=116800

app = Quart(__name__)
client = discord.Client()

cache_breaker = 1
config = toml.load('config.toml')

SGFS = {}  # type: Dict[str, SGF]


class SGF:
    def __init__(self, board: Board, plays: List[Tuple[str, Tuple[int, int]]], checksum: str) -> None:
        self.board = board
        self.plays = plays
        self.checksum = checksum


class ReactionManager:
    instance = None  # type: Optional[ReactionManager]

    def __init__(self) -> None:
        self.listeners = []  # type: List[Callable[[discord.Reaction, discord.User], Coroutine[Any, Any, None]]]

    def add_listener(self, coro) -> None:
        self.listeners.append(coro)

    def on_reaction_change(self, reaction: discord.Reaction, user: discord.User) -> None:
        tasks = [asyncio.create_task(listener(reaction, user)) for listener in self.listeners]
        asyncio.gather(*tasks)

    @staticmethod
    def get_instance() -> "ReactionManager":
        if ReactionManager.instance is None:
            ReactionManager.instance = ReactionManager()
        return ReactionManager.instance


def make_uri(checksum: str, move: int) -> str:
    # return config["general"]["base_url"] + url_for('board_route', checksum=checksum, move=move)
    return f'{config["general"]["base_url"]}/{checksum}/{move}'


def sha1(s: bytes) -> str:
    return hashlib.sha1(s).hexdigest()


@app.route("/<string:checksum>/<int:move>")
def board_route(checksum: str, move: int):
    sgf_data = SGFS[checksum]
    if move > len(sgf_data.plays):
        move = len(sgf_data.plays)
    board = get_board(sgf_data, move)
    svg = board_to_svg(board, sgf_data.plays[move - 1][1])
    png = cairosvg.svg2png(bytestring=svg, scale=15)
    return Response(png, mimetype="image/png")


def get_board(sgf_data: SGF, move: int) -> Board:
    board = sgf_data.board.copy()
    plays = sgf_data.plays
    for i in range(move):
        try:
            (colour, (row, col)) = plays[i]
            board.play(row, col, colour)
        except TypeError:
            # Player passed
            pass
    return board


def board_to_svg(board: Board, last_move: Tuple[int, int]) -> str:
    # SVG Header
    ret = '<svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">'

    # Background
    ret += '<rect x="0" y="0" width="20" height="20" fill="white" />'

    # Lines
    for x in range(19):
        ret += f'<line x1="{x + 1}" y1="1" x2="{x + 1}" y2="19" stroke="black" stroke-width="0.1" />'
    for y in range(19):
        ret += f'<line x1="1" y1="{y + 1}" x2="19" y2="{y + 1}" stroke="black" stroke-width="0.1" />'

    # Hoshi
    hoshi_positions = [
        (4, 4), (10, 4), (16, 4),
        (4, 10), (10, 10), (16, 10),
        (4, 16), (10, 16), (16, 16)]
    for hoshi in hoshi_positions:
        ret += f'<circle cx="{hoshi[0]}" cy="{hoshi[1]}" r="0.2" fill="black" />'

    # Stones
    for row in range(19):
        for col in range(19):
            colour = board.get(row, col)
            if colour == 'w':
                ret += f'<circle cx="{col + 1}" cy="{18 - row + 1}" r="0.45" fill="white" stroke="black" stroke-width="0.1" />'
            elif colour == 'b':
                ret += f'<circle cx="{col + 1}" cy="{18 - row + 1}" r="0.5" fill="black" />'
            if last_move == (row, col):
                if colour == 'w':
                    ret += f'<circle cx="{col + 1}" cy="{18 - row + 1}" r="0.2" fill="none" stroke="black" stroke-width="0.1" />'
                elif colour == 'b':
                    ret += f'<circle cx="{col + 1}" cy="{18 - row + 1}" r="0.2" fill="none" stroke="white" stroke-width="0.1" />'
    ret += '</svg>'
    return ret


async def reaction_handler(mes: discord.Message, sgf_data: SGF) -> None:
    first = "\N{BLACK LEFT-POINTING DOUBLE TRIANGLE WITH VERTICAL BAR}"
    left10 = "\N{BLACK LEFT-POINTING DOUBLE TRIANGLE}"
    left = "\N{LEFTWARDS BLACK ARROW}"
    right = "\N{BLACK RIGHTWARDS ARROW}"
    right10 = "\N{BLACK RIGHT-POINTING DOUBLE TRIANGLE}"
    last = "\N{BLACK RIGHT-POINTING DOUBLE TRIANGLE WITH VERTICAL BAR}"
    await mes.add_reaction(first)
    await mes.add_reaction(left10)
    await mes.add_reaction(left)
    await mes.add_reaction(right)
    await mes.add_reaction(right10)
    await mes.add_reaction(last)

    max_move = len(sgf_data.plays)
    move_no = 0

    def check(reaction, user):
        if user.id == client.user.id:
            return False
        if reaction.message.id != mes.id:
            return False
        return True

    async def on_reaction(reaction, user) -> None:
        nonlocal move_no
        nonlocal max_move
        if not check(reaction, user):
            return

        if reaction.emoji == first:
            move_no = 0
        elif reaction.emoji == left10:
            move_no = max(0, move_no - 10)
        elif reaction.emoji == left:
            move_no = max(0, move_no - 1)
        elif reaction.emoji == right:
            move_no = min(max_move, move_no + 1)
        elif reaction.emoji == right10:
            move_no = min(max_move, move_no + 10)
        elif reaction.emoji == last:
            move_no = max_move
        else:
            return

        embed = discord.Embed().set_image(url=make_uri(sgf_data.checksum, move_no))
        await mes.edit(embed=embed)
    ReactionManager.get_instance().add_listener(on_reaction)


@client.event
async def on_message(message: discord.Message) -> None:
    if message.author.id == client.user.id:
        return

    for attachment in message.attachments:
        if attachment.filename.endswith(".sgf"):
            sgf_file = await attachment.read()
            checksum = sha1(sgf_file)
            if checksum in SGFS:
                break
            board, plays = sgf_moves.get_setup_and_moves(sgf.Sgf_game.from_bytes(sgf_file))
            sgf_data = SGF(board, plays, checksum)
            SGFS[checksum] = sgf_data
            mes = await message.channel.send(embed=discord.Embed().set_image(url=make_uri(checksum, 0)))
            await reaction_handler(mes, sgf_data)
            break


@client.event
async def on_reaction_add(reaction: discord.Reaction, user: discord.User) -> None:
    ReactionManager.get_instance().on_reaction_change(reaction, user)


@client.event
async def on_reaction_remove(reaction: discord.Reaction, user: discord.User) -> None:
    ReactionManager.get_instance().on_reaction_change(reaction, user)


@client.event
async def on_ready():
    print("Ready")


loop = asyncio.get_event_loop()
loop.create_task(client.start(config["discord"]["token"]))
loop.run_until_complete(serve(app, hypercorn.config.Config.from_toml("hypercorn.toml")))
